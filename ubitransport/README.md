# ubitransport
## Requirements
npm >= 6.14.5
Node.js >= 14.5
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Features

### Switch month
Select the desired month in the first dropdown menu.<br/>
It contains the next 100 months from current date.

### Select a single date

Click on a cell to select a single date.<br/>
Click again to unselect.

### Select multiple date
Maintain left mouse button on a cell and drag the cursor to the adjacents cells to select them as well.

### Select every day of the week from a given month
Select the desired day of the week from the second dropdown menu, it will set the selection for this day only.

## Tests
```
npm run test:unit
```