import { shallowMount } from "@vue/test-utils";
import SelectMonth from "../../components/SelectMonth.vue";

const factory = (values = {}) => {
  return shallowMount(SelectMonth, {
    data() {
      return {
        ...values
      };
    }
  });
};

describe("SelectMonth", () => {
  it("Contains 100 dates",()=>{
    const wrapper=factory()
    expect(wrapper.vm.monthList.length).toBe(100)
  })
});
