import { shallowMount } from "@vue/test-utils"
import CalendarCell from "../../components/CalendarCell.vue"
import moment from 'moment'

const date = moment(new Date(2020,1,1)) // Mercredi 1er janvier 2020 

const factory = ({values = {},propsData ={}}) => {
  return shallowMount(CalendarCell, {
    data() {
      return {
        ...values
      };
    },
    propsData:{
        ...propsData
    }
  });
};

describe("CalendarCell", () => {
  it("Is selected if weekday is the same as selected weekday on mount",()=>{
    const wrapper=factory({propsData:{day: date, selectedDayOfWeek:{dayNumber:date.day()}}})
    expect(wrapper.vm.isSelected).toBe(true)
  }),
  it("Is not selected if weekday is not the same as selected weekday on mount",()=>{
    const wrapper=factory({propsData:{day: date, selectedDayOfWeek:{dayNumber:0}}})
    expect(wrapper.vm.isSelected).toBe(false)
  }),
  it("Is selected if weekday change to be the same day", async ()=>{
    const wrapper=factory({propsData:{day: date, selectedDayOfWeek:{dayNumber:0}}})
    expect(wrapper.vm.isSelected).toBe(false)
    await wrapper.setProps({selectedDayOfWeek:{dayNumber: date.day()}})
    expect(wrapper.vm.isSelected).toBe(true)
  }),
  it("Is selected / unselected on click", async ()=> {
    const wrapper=factory({propsData:{day: date, selectedDayOfWeek:{dayNumber:0}}})
    expect(wrapper.vm.isSelected).toBe(false)
    await wrapper.trigger('mousedown')
    expect(wrapper.vm.isSelected).toBe(true)
    await wrapper.trigger('mousedown')
    expect(wrapper.vm.isSelected).toBe(false)
  }),
  it("Is selected on slide (hover + mouseDown)", async ()=>{
    const wrapper=factory({propsData:{day: date, selectedDayOfWeek:{dayNumber:0}, isMouseDown: true}})
    expect(wrapper.vm.isSelected).toBe(false)
    await wrapper.trigger('mouseover')
    expect(wrapper.vm.isSelected).toBe(true)
  })
});
