import { shallowMount } from "@vue/test-utils";
import Calendar from "../../components/Calendar.vue";


const factory = (values = {}, propsData = {}) => {
  return shallowMount(Calendar, {
    data() {
      return {
        ...values
      };
    },
    propsData:{
        ...propsData
    }
  });
};

describe("Calendar", () => {
  it("Calendar contain the expected number of days",async ()=>{
    const wrapper = factory({},{month:{month:0,year:2020}})
    expect(wrapper.vm.daysList.length).toBe(31)
  })
});
